package funcGoupByTest;

import functions.GroupByRegionFunc;
import functions.reader.CsvFileReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GroupByRegionTest {

    @Test
    public void testGroupByRegion(){
        CsvFileReader reader = new CsvFileReader("src\\test\\resources\\test_fete_musique.csv");
        Dataset<Row> actual = reader.get();

        Dataset<Row> ds = new GroupByRegionFunc().apply(actual);

        assertThat(ds.first().toString()).contains("Hauts-de-France");
    }
}
