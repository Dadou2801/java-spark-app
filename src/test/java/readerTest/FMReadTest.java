package readerTest;

import functions.reader.CsvFileReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class FMReadTest {

    @Test
    public void testCsvFileReader(){
        CsvFileReader reader = new CsvFileReader("src\\test\\resources\\test_fete_musique.csv");
        Dataset<Row> fm = reader.get();

        long actual = fm.count();
        int expected = 2;

        assertThat(actual).isNotZero();
        assertThat(actual).isEqualTo(expected);
    }
}
