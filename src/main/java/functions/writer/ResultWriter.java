package functions.writer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class ResultWriter implements Consumer<Dataset<Row>> {
    private final String outputPathStr;

    @Override
    public void accept(Dataset<Row> rowDataset) {

        rowDataset
                .coalesce(1)
                .write()
                .mode(SaveMode.Overwrite)
                .csv(outputPathStr);
    }
}
