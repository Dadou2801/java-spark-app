package functions.reader;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.function.Supplier;

@RequiredArgsConstructor
public class CsvFileReader implements Supplier<Dataset<Row>> {
    private final String inputPathStr;
    private final SparkSession sparkSession;

    public CsvFileReader(String inputPathStr){
        this.inputPathStr = inputPathStr;
        SparkConf sparkConf = new SparkConf().setMaster("local[2]").setAppName("FMApp");
        this.sparkSession = SparkSession.builder().config(sparkConf).getOrCreate();
    }

    @Override
    public Dataset<Row> get(){
        Dataset<Row> fm = sparkSession.read()
                .option("delimiter", ";")
                .option("header", "true")
                .csv(this.inputPathStr);
        return fm;
    }

}
