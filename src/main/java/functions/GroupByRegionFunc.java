package functions;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.function.Function;

import static org.apache.spark.sql.functions.count;

@RequiredArgsConstructor
public class GroupByRegionFunc implements Function<Dataset<Row>, Dataset<Row>> {

    @Override
    public Dataset<Row> apply(Dataset<Row> rowDataset){
        Dataset<Row> ds = rowDataset.
                groupBy("location_region")
                .agg(count("uid")
                        .as("Occurences"));

        return ds;
    }
}
