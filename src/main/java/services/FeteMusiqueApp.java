package services;

import functions.GroupByRegionFunc;
import functions.reader.CsvFileReader;
import functions.writer.ResultWriter;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;


import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;


@Slf4j
public class FeteMusiqueApp {

    public static void main( String[] args ) {
        Config config = ConfigFactory.load();
        String inputPathStr = config.getString("3il.path.input");
        String outputPathStr = config.getString("3il.path.output");

        //log.info( "Music party");

        SparkConf sparkConf = new SparkConf().setMaster("local[2]").setAppName("FMApp");
        SparkSession sparkSession = SparkSession.builder().config(sparkConf).getOrCreate();

        CsvFileReader reader = new CsvFileReader(inputPathStr, sparkSession);
        GroupByRegionFunc groupByRegionFunc = new GroupByRegionFunc();
        ResultWriter resultWriter = new ResultWriter(outputPathStr + "/groupByRegionFunc");

        Dataset<Row> lines = reader.get();
        Dataset<Row> gbRegion = groupByRegionFunc.apply(lines);

        resultWriter.accept(gbRegion);

        try {
            Thread.sleep(1000*60*10);
        }catch (InterruptedException interruptedException){
            interruptedException.printStackTrace();
        }
    }
}
